In \cite{xref:/book/book}, it has been proven that the membership functions defined by (\ref{miu4}) are also coherent membership functions and the
membership function defined by (\ref{miu3}) converges to that defined by (\ref{miu4}), for all $x\in \Omega$
as $|X|$ approaches to infinity. This ensures that the laws discovered based on the membership functions and their
logic operations determined by the finite observed data $X$ (\ref{miu3}) can be extended to the whole space $\Omega$ by membership functions (\ref{miu4}) and analyzed in the framework of probability theory.


In (\ref{miu3}, \ref{miu4}), the membership degree of $x \in X$ belonging to the fuzzy concept $\xi=\sum_{i\in I}(\Pi_{m\in
A_{i}}m)$ is determined by both $\rho_{\gamma}(x)$ and $A^{\succeq}_{i}(x)$. $\rho_{\gamma}(x)$ is dependent on the semantic interpretation of fuzzy term $\gamma$ and $A^{\succeq}_{i}(x)$ is dependent both the semantic meanings of fuzzy terms in $A_{i}$ and the probability distribution of the feature values of data $X$. Thus the coherence membership function takes both fuzziness and randomness into account.


\section{Fuzzy Entropy}
Entropy is a measure of uncertainty in terms of outcome for a random experiment,
or equivalently, a measure of information obtained when a random
outcome is observed. This concept has been defined in various ways \cite{xref:/journal/Y36,xref:/journal/Y37,xref:/journal/Y38,xref:/journal/Y39,xref:/book/Y40,xref:/conference/Y41} and extended in different areas, such as communication, mathematics, statistical thermodynamics etc. In what follows, we first briefly introduce the
concept of Shannon's entropy and then go to the membership entropy, which will be used in this paper.

\subsection{Shannon's Entropy}
Let $X$ be a discrete random variable with a finite set containing $N$ symbols
$x_{0}, x_{1}, \ldots, x_{N}$. If an output $x_{j}$ occurs with probability $p(x_{j})$, then the
amount of information associated with the known occurrence of the output $x_{j}$ is defined as
\begin{equation}
I(x_{j}) = -log_{2} p(x_{j})
\end{equation}
Based on this, the concept of Shannon's entropy is defined as follows:
)))))~~~~~

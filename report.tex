\documentclass[a4paper,titlepage,12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[margin=1in]{geometry}
\usepackage[figuresright]{rotating}
\usepackage{parskip}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{amsmath}
\usepackage{amssymb}
\hypersetup{
	hidelinks,
	pdfauthor=Delan Azabani,
	pdftitle=Fundamental Concepts of Cryptography: Assignment 2
}
\lstset{basicstyle=\ttfamily, basewidth=0.5em}

\DeclareMathOperator{\fcch}{H}

\title{Fundamental Concepts of Cryptography:\\Assignment 2}
\date{June 2, 2015}
\author{Delan Azabani}

\pagenumbering{gobble}

\begin{document}

\maketitle

\pagenumbering{roman}
\tableofcontents
\newpage
\pagenumbering{arabic}

\section{Modular exponentiation}

\subsection{Python implementation}

\begin{lstlisting}
def modexp(a, b, n):
	"""Compute [y = (a ^ b) mod n] efficiently."""
	y = 1 # (a ^ b) mod n
	i = 0 # bit index into b from LSB 0 to MSB k
	A = a # (a ^ (2 ^ i)) mod n
	B = b # b without its least significant i bits
	while B > 0:
		if B & 1:
			y *= A
			y %= n
		A *= A
		A %= n
		B >>= 1
		i += 1
	return y
\end{lstlisting}

\subsection{Required demonstration}

\begin{lstlisting}
% python
Python 2.7.6 (default, Sep  9 2014, 15:04:36)
[GCC 4.2.1 Compatible Apple LLVM 6.0 (clang-600.0.39)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> import rsa
>>> rsa.modexp(17, 21, 39)
38
\end{lstlisting}

\newpage

\section{RSA encryption and decryption}

\subsection{Python implementation}

\lstinputlisting{rsa.py}

\newpage

% (p, q) = (9349, 1451)
% (e, n) = (10231127, 13565399)
% (d, n) = (2624063, 13565399)

\subsection{Start of encrypted output: $(e, n) = (10231127, 13565399)$}

\input{test.cipher.first.txt}

\newpage

\subsection{End of encrypted output: $(e, n) = (10231127, 13565399)$}

\input{test.cipher.last.txt}

\newpage

\subsection{Start of decrypted output: $(d, n) = (2624063, 13565399)$}

\lstset{breaklines=true}
\lstinputlisting{test.plain.first.txt}

\newpage

\subsection{End of decrypted output: $(d, n) = (2624063, 13565399)$}

\lstset{breaklines=true}
\lstinputlisting{test.plain.last.txt}

\newpage

\section{DSS signature forgery}

\subsection{Algorithm description}

To sign a message using the DSS, the following are common public knowledge:

\begin{itemize}
	\item $L$ --- a bit length where
	      $L\in\left\{512,576,\dots,1024\right\}$
	\item $N$ --- a bit length shorter than the output of
	      the message digest function $\fcch\left(M\right)$
	\item $p$ --- a large prime number where
	      $\left\lceil\log_2p\right\rceil=L$
	\item $q$ --- a prime divisor of $(p-1)$ where
	      $\left\lceil\log_2q\right\rceil=N$
	\item $h$ --- an integer in $\left(1,p-1\right)$ where
	      $h^{\left(p-1\right)/q}\mod p>1$
	\item $g$ --- the integer $h\left(p-1\right)/q\mod p$
\end{itemize}

The following is private for each user who wishes to create signatures:

\begin{itemize}
	\item $x$ --- a random integer where $0<x<q$
\end{itemize}

The following is unique to each user, but is public information:

\begin{itemize}
	\item $y$ --- the integer $g^x\mod p$
\end{itemize}

The following is generated and kept secret for each message being signed:

\begin{itemize}
	\item $k$ --- a random integer where $0<k<q$
\end{itemize}

Signatures conceptually take the form $\left(r,s\right)$ where:

\begin{itemize}
	\item $r=\left(g^k\mod p\right)\mod q$
	\item $s=\left(k^{-1}\left(\fcch\left(M\right)+xr\right)\right)\mod q$
\end{itemize}

\subsection{How a second preimage can lead to an existential forgery}

Having found a colliding preimage $M'$ where
$\fcch\left(M'\right)=\fcch\left(M\right)$, Bob can trivially pose the
signed message $\left(M',r,s\right)$ as having been signed by Alice,
without requiring access to $k$ or any of Alice's private key material.
The signature need not be modified.

This is because $\fcch\left(M\right)$ is the only component of a DSS
signature that is a function of the message, and given colliding
messages, the result of said function on $M'$ is identical.

Despite this, an existential forgery ``may only be a minor nuisance''
to Alice \footnotemark{} depending on the context in which DSS is used,
and is a significantly less severe threat than allowing Bob to
engage in selective or universal forgery. Furthermore, Bob's discovery
of a second preimage to the message digest does not provide Bob with
useful private key material that may help him with a total break.

\footnotetext{Stallings, W. (2014). \textit{Cryptography and Network
Security: Principles and Practice.} Upper Saddle River, New Jersey:
Pearson Education, Inc.}

\newpage

\section{Proof of the specified assertion}

Given a prime number $p$ and a positive integer $a$ between $1$ and
$p-1$ inclusive, if $k$ is the order of $a$, then $k$ is a factor of
$p-1$. The following is a proof of the assertion.

\begin{enumerate}
	\item\textit{Axiom:} As a prime number,
	     $p\in\mathbb Z\cap\left[2,\infty\right)$
	     and its factors are $\pm1$ and $\pm p$.
	\item\textit{Assertion:} $1\leq a<p$ as initially defined.
	\item\textit{From \#2:} $p$ is definitely not a factor of $a$.
	\item\textit{From \#1 and \#3:} $1$ is the only common factor
	     shared by $p$ and $a$.
	\item\textit{From \#4:} $p$ and $a$ are relatively prime.
	\item\textit{Euler's theorem:} For every $p$ and $a$ that are
	     relatively prime, $a^{\phi\left(p\right)}\equiv1\mod p$.
	\item\textit{Axiom:} $\phi\left(p\right)=p-1$ given a prime
	     number $p$.
	\item\textit{From \#6 and \#7:} The order of $p$ must not
	     exceed $p-1$.
	\item\textit{From \#8:} Either $k=p-1$, where $k$ is trivially
	     a factor of $p-1$, or $0<k<p-1$, where the sequence of
	     exponents of $a$ modulo $p$ would repeat.
	\item\textit{From \#9:} Where $0<k<p-1$ holds, continuing to
	     satisfy \#6 requires that this sequence repeats an
	     integral number of times.
	\item\textit{From \#10:} $p-1=kn$ for an arbitrary
	     $n\in\mathbb Z$.
	\item\textit{Axiom:} The integers are closed under
	     multiplication.
	\item\textit{From \#11 and \#12:} $k$ is a factor of $p-1$.
	     $\quad\square$
\end{enumerate}

\end{document}

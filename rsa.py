#!/usr/bin/env python

import sys
import random

R = random.SystemRandom()

def modexp(a, b, n):
	"""Compute [y = (a ^ b) mod n] efficiently."""
	y = 1 # (a ^ b) mod n
	i = 0 # bit index into b from LSB 0 to MSB k
	A = a # (a ^ (2 ^ i)) mod n
	B = b # b without its least significant i bits
	while B > 0:
		if B & 1:
			y *= A
			y %= n
		A *= A
		A %= n
		B >>= 1
		i += 1
	return y

def lehmann(p, t=1000):
	"""Probabilistically determine primality using the Lehmann test.

	Returns the probability of p being prime after t iterations.
	"""
	for i in range(t):
		a = R.randint(0, p - 1)
		r = modexp(a, (p - 1) / 2, p)
		if r not in (1, p - 1):
			return 0.0
	return 1.0 - 2.0 ** -t

def pair(a=1000, b=10000, t=1000):
	"""Generate a pair of random primes a <= (p, q) <= b.

	Returns (p, q) as a tuple tested to lehmann(t).
	"""
	p, q = a, b # decorative
	while not lehmann(p, t):
		p = R.randint(a, b)
	while not lehmann(q, t):
		q = R.randint(a, b)
	return (p, q)

def euclid(a, b):
	"""Compute the extended Euclidean algorithm.

	Returns (d, x, y) as a tuple given [d = gcd(a, b) = ax + by].
	"""
	if a < 0: return euclid(-a, b)
	if b < 0: return euclid(a, -b)
	if a < b: return euclid(b, a)
	Q, R, X, Y = None, a, 1, 0
	q, r, x, y = None, b, 0, 1
	i = 0
	while r > 0:
		Q, R, q, r = q, r, R / r, R % r
		X, Y, x, y = x, y, X - q * x, Y - q * y
		i += 1
	return (R, X, Y)

def key(p, q):
	"""Generate a RSA key pair ((e, n), (d, n)) given (p, q)."""
	n = p * q
	N = n - p - q + 1
	e = N
	while euclid(N, e)[0] != 1:
		e = R.randint(2, N - 1)
	d = euclid(N, e)[2] % N
	return ((e, n), (d, n))

def encrypt(key, M):
	return modexp(M, *key)

def decrypt(key, C):
	return modexp(C, *key)

def ecb_encrypt(key):
	byte = sys.stdin.read(1)
	while byte:
		print encrypt(key, ord(byte)),
		byte = sys.stdin.read(1)
	print

def ecb_decrypt(key):
	for line in sys.stdin:
		for C in line.split():
			sys.stdout.write(chr(decrypt(key, int(C))))

if __name__ == '__main__':
	if len(sys.argv) not in (2, 4) or sys.argv[1] not in ('enc', 'dec'):
		print 'Usage: %s [enc | dec] [[e | d] [n]]' % sys.argv[0]
		print '17 ^ 21 = %d mod 39' % modexp(17, 21, 39)
		sys.exit()
	if len(sys.argv) == 4:
		k = (int(sys.argv[2]), int(sys.argv[3]))
	else:
		k = None
		p, q = pair()
		public, private = key(p, q)
		sys.stderr.write((
			'(p, q) = (%d, %d)\n' +
			'(e, n) = (%d, %d)\n' +
			'(d, n) = (%d, %d)\n'
		) % (
			p, q,
			public[0], public[1],
			private[0], private[1]
		))
	if sys.argv[1] == 'enc':
		ecb_encrypt(k or public)
	elif sys.argv[1] == 'dec':
		ecb_decrypt(k or private)

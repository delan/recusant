\subsection{AFS Algebras}
###&&&
The Iris dataset is used as an illustrative example for AFS algebras through
this paper. It has 150 samples which are evenly distributed in three
classes and 4 features of sepal length($f_1$), sepal
width($f_2$), petal length($f_3$), and petal width($f_4$). Let a
pattern $x=(x_{1},x_{2},x_{3},x_{4})$, where $x_{i}$ is the $i$th
feature value of $x$. The following three linguist fuzzy rules have been obtained for Class 1 to build the AFS fuzzy classifier in Section 4.

\bigskip
\textbf{Rule} $R_1$: If $x_{1}$ is \emph{short sepal}, and $x_{2}$
is \emph{wide sepal}, and $x_{4}$ is \emph{narrow petal}, then $x$
belongs to Class 1;

\textbf{Rule} $R_2$: If $x_{2}$ is \emph{wide sepal} and $x_{3}$ is
\emph{short petal}, then $x$ belongs to Class 1;

\textbf{Rule} $R_3$: If $x_{1}$ is \emph{short sepal} and $x_{4}$ is
\emph{narrow petal}, then $x$ belongs to Class 1;

\bigskip

 Let $M=\{m_{j,k}|1\leq j\leq
4, 1\leq k\leq 3\}$ be the set of fuzzy concepts, where $%
m_{j,1},m_{j,2},m_{j,3}$ are fuzzy terms ``\emph{large}",
``\emph{small}", ``\emph{medium}" associated with feature $f_j$
respectively, then the above linguist fuzzy rules can be written in the
following form:

\bigskip
\textbf{Rule} $R_1$ : If $x$ is ``$m_{1,2}$ and $m_{2,1}$ and
$m_{4,2}$", then $x$ belongs to Class 1;

\textbf{Rule} $R_2$ : If $x$ is ``$m_{2,1}$ and $m_{3,2}$", then $x$
belongs to Class 1;

\textbf{Rule} $R_3$ : If $x$ is ``$m_{1,2}$ and $m_{4,2}$", then $x$
belongs to Class 1.

\bigskip

For each set of fuzzy terms, $A \subseteq M$, $\prod_{m\in
A}m$ represents a conjunction of the fuzzy terms in $A$. For
instance,
